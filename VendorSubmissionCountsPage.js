import React, { Component } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";

class CorrespondenceLookup extends Component {
  render() {
    return (
      <Form>
        <br />
        <h4>Vendor Submission Counts Page</h4>
        <br></br>
        <p>Please select the month for which you want the vendor counts</p>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
          Month
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option>2019-02</option>
              <option>2019-01</option>
              <option>2018-12</option>
              <option>2018-11</option>
              <option>2018-10</option>
              <option>2018-09</option>
              <option>2018-08</option>
              <option>2018-07</option>
              <option>2018-06</option>
              <option>2018-05</option>
              <option>2018-04</option>
              <option>2018-03</option>
              <option>2018-02</option>
              <option>2018-01</option>
              <option>2017-12</option>
              <option>2017-11</option>
              <option>2017-10</option>
              <option>2017-09</option>
              <option>2017-08</option>
              <option>2017-07</option>
              <option>2017-06</option>
              <option>2017-05</option>
              <option>2017-04</option>
              <option>2017-03</option>
              <option>2017-02</option>
              <option>2017-01</option>
              <option>2016-12</option>
              <option>2016-11</option>
              <option>2016-10</option>
              <option>2016-09</option>
              <option>2016-08</option>
              <option>2016-07</option>
              <option>2016-06</option>
              <option>2016-05</option>
              <option>2016-04</option>
              <option>2016-03</option>
              <option>2016-02</option>
              <option>2016-01</option>
              <option>2015-12</option>
              <option>2015-11</option>
              <option>2015-10</option>
              <option>2015-09</option>
              <option>2015-08</option>
              <option>2015-07</option>
              <option>2015-06</option>
              <option>2015-05</option>
              <option>2015-04</option>
              <option>2015-03</option>
              <option>2015-02</option>
              <option>2015-01</option>
              <option>2014-12</option>
              <option>2014-11</option>
              <option>2014-10</option>
              <option>2014-09</option>
              <option>2014-08</option>
              <option>2014-07</option>
              <option>2014-06</option>
              <option>2014-05</option>
              <option>2014-04</option>
              <option>2014-03</option>
              <option>2014-02</option>
              <option>2014-01</option>
              <option>2013-12</option>
              <option>2013-11</option>
              <option>2013-10</option>
              <option>2013-09</option>
              <option>2013-08</option>
              <option>2013-07</option>
              <option>2013-06</option>
              <option>2013-05</option>
              <option>2013-04</option>
              <option>2013-03</option>
              <option>2013-02</option>
              <option>2013-01</option>
              <option>2012-12</option>
              <option>2012-11</option>
              <option>2011-10</option>
              <option>2011-09</option>
              <option>2011-08</option>
              <option>2011-07</option>
              <option>2011-06</option>
              <option>2011-05</option>
              <option>2011-04</option>
              <option>2011-03</option>
              <option>2011-02</option>
              <option>2011-01</option>
              <option>2010-12</option>
              <option>2010-11</option>
              <option>2010-10</option>
              <option>2010-09</option>
              <option>2010-08</option>
              <option>2010-07</option>
              <option>2010-06</option>
              <option>2010-05</option>
              <option>2010-04</option>
              <option>2010-03</option>
              <option>2010-02</option>
              <option>2010-01</option>
              <option>2009-12</option>
              <option>2009-11</option>
              <option>2009-10</option>
              <option>2009-09</option>
              <option>2009-08</option>
              <option>2009-07</option>
              <option>2009-06</option>
              <option>2009-05</option>
              <option>2009-04</option>
              <option>2009-03</option>
              <option>2009-02</option>
              <option>2009-01</option>
              <option>2008-12</option>
              <option>2008-11</option>
              <option>2008-10</option>
              <option>2008-09</option>
              <option>2008-08</option>
              <option>2008-07</option>
              <option>2008-06</option>
              <option>2008-05</option>
              <option>2008-04</option>
              <option>2008-03</option>
              <option>2008-02</option>
              <option>2008-01</option>
              <option>2007-12</option>
              <option>2007-11</option>
              <option>2007-10</option>
              <option>2007-09</option>
              <option>2007-08</option>
              <option>2007-07</option>
              <option>2007-06</option>
              <option>2007-05</option>
              <option>2007-04</option>
              <option>2007-03</option>
              <option>2007-02</option>
              <option>2007-01</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Button variant="primary" type="submit">
          Lookup
        </Button>
      </Form>
    );
  }
}

export default CorrespondenceLookup;
