import React, { Component } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";

class CorrespondenceLookup extends Component {
  render() {
    return (
      <Form>
        <br />
        <h2>Correspondence Lookup</h2>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
          Originator Site
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter  Originator Site"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
          Representative
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Representative"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
          Process Indicator
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder="Choose Process Indicator">
              <option />
              <option>Send</option>
              <option>Resend </option>
              <option>QA</option>
              <option>Hold</option>
            </Form.Control>
            </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
          Type
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option>Bulk</option>
              <option>Email </option>
              <option>Fax</option>
              <option>Print</option>
              <option>SMS</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
          Body Template
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Body Template"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
          Attach. Template
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Attach. Template"
            />
            <br></br>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Transaction Number
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Transaction Number"
            />
             </Col>
             *
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Document Number
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Document Number"
            />
             </Col>
             *
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Member Number
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Member Number"
            />
             </Col>
             *
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Recipient 
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Recipient"
            />
             </Col>
             *
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Batch Number
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Batch Number"
            />
             </Col>
             *
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Printer
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Printer"
            />
          </Col>
        </Form.Group>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Language
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option>English</option>
              <option>Afrikaans </option>
              <option>Portoguese</option>
            </Form.Control>
            <br></br>
            </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            From Date
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter From Date"
            />
             </Col>
             * Format: YYYY-MM-DD OR YYYY-MM-DD hh:mm:ss
        </Form.Group>
        
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
           To Date
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter To Date"
            />
          </Col>
          * Format: YYYY-MM-DD OR YYYY-MM-DD hh:mm:ss
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
          Limit
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option>Unlimited</option>
              <option>10 </option>
              <option>50</option>
              <option>100</option>
              <option>200</option>
              <option>500</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Button variant="primary" type="submit">
          Lookup
        </Button>
      </Form>
    );
  }
}

export default CorrespondenceLookup;
