import React, { Component } from "react";

class Dashboard extends Component {
  constructor() {
    super();
    this.state = {
      queries: []
    };
  }

  componentDidMount() {
    fetch("http://localhost:8585/querydetails")
      .then(response => response.json())
      .then(response => this.setState({ queries: response.data }))
      .catch(err => console.error(err));
  }

  render() {
    return (
      <div className="container">
        <div className="jumbotronmt-5">
          <div className="col-sm-8 mx-auto">
            <br />
            <h1 className="text-center">
              <font color="#06b1e5">DASHBOARD</font>
            </h1>
          </div>
          <table className="table col-md-6 mx-auto">
            <thead>
              <tr>
                <th>
                  <font color="#06b1e5">Reference ID</font>
                </th>
                <th>
                  <font color="#06b1e5">Member Number</font>
                </th>
                <th>
                  <font color="#06b1e5">Provider Number</font>
                </th>
                <th>
                  <font color="#06b1e5">Phone Number</font>
                </th>
                <th>
                  <font color="#06b1e5">Type of Enquiry</font>
                </th>
                <th>
                  <font color="#06b1e5">Query Details</font>
                </th>
                <th>
                  <font color="#06b1e5">Start Time</font>
                </th>
                <th>
                  <font color="#06b1e5">End Time</font>
                </th>
                <th>
                  <font color="#06b1e5">Employee ID</font>
                </th>
                <th> </th>
              </tr>
            </thead>
            <tbody>
              {this.state.queries.map(query => (
                <tr key={query.id}>
                  <td>{query.Reference_ID} </td>
                  <td>{query.Member_No}</td>
                  <td>{query.Provider_No}</td>
                  <td>{query.Phone_Number}</td>
                  <td>{query.Type_of_Enquiry}</td>
                  <td>{query.Query_Details}</td>
                  <td>{query.StartTime}</td>
                  <td>{query.EndTime}</td>
                  <td>{query.Employee_ID}</td>
                  <td>
                    <a href="#">Edit</a>|<a href="#">Delete</a>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default Dashboard;
