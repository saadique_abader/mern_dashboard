import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { Dropdown } from "react-bootstrap";
class Navbar extends Component {
  logout(e) {
    e.preventDefault();
    localStorage.removeItem("usertoken");
    this.props.history.push("/");
  }

  render() {
    const loginRegLink = (
      <ul className="navbar-nav">
        <li className="nav-item">
          <Link to="/" className="nav-link">
            <font color=" #FFFFFF">
              <strong>Home</strong>
            </font>
          </Link>
        </li>
        <li className="nav-item">
          <Link to="/login" className="nav-link">
            <font color=" #FFFFFF">
              <strong>Login</strong>
            </font>
          </Link>
        </li>
        <li className="nav-item">
          <Link to="/register" className="nav-link">
            <font color=" #FFFFFF">
              <strong>Register</strong>
            </font>
          </Link>
        </li>
      </ul>
    );
    const UserLink = (
      <ul className="navbar-nav">
        <li className="nav-item">
          <Link to="/home" className="nav-link">
            <font color=" #FFFFFF">
              <strong>Home</strong>
            </font>
          </Link>
        </li>
        <li className="nav-dropdown">
          <Link to="/profile" className="nav-link">
            <font color=" #FFFFFF">
              <strong>User</strong>
            </font>
          </Link>
        </li>
        <Dropdown>
          <Dropdown.Toggle variant="info" id="dropdown-basic" color=" #FFFFFF">
            <strong>Queries</strong>
          </Dropdown.Toggle>

          <Dropdown.Menu>
            <Dropdown.Item href="/submissionlookup">
              Submission Lookup
            </Dropdown.Item>
            <Dropdown.Item href="#/action-2">Submission Detail</Dropdown.Item>
            <Dropdown.Item href="#/action-3">
              Correspondence Lookup
            </Dropdown.Item>
            <Dropdown.Item href="#/action-1">Data Load Lookup</Dropdown.Item>
            <Dropdown.Item href="#/action-2">Data Load Detail</Dropdown.Item>
            <Dropdown.Item href="#/action-3">
              Vendor Submission Counts
            </Dropdown.Item>
            <Dropdown.Item href="#/action-1">Datasync Check</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>

        <li className="nav-item">
          <a href=" " onClick={this.logout.bind(this)} className="nav-link">
            <font color=" #FFFFFF">
              <strong>Logout</strong>
            </font>
          </a>
        </li>
      </ul>
    );
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-info variant-dark">
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbar1"
          aria-controls="navbar1"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggle-icon" />
        </button>

        <div
          className="collapse navbar-collapse justify-content-md-center"
          id="navbar1"
        >
          {localStorage.usertoken ? UserLink : loginRegLink}
        </div>
      </nav>
    );
  }
}
export default withRouter(Navbar);
