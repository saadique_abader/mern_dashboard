import React, { Component } from "react";
import logo from "../logo_sml.png";

class Landing extends Component {
  render() {
    return (
      <div className="container">
        <div className="jumbotron mt-5">
          <div className="col-sm-8 mx-auto">
            <h1 className="text-center">
              <font color="#06b1e5">WELCOME TO THE ZIETO LOGIN PAGE</font>
            </h1>
            <img src={logo} fluid />
            <br />
            <h5 className="text-center">
              Are you a user? Log in{" "}
              <a href="/login">
                <font color="#06b1e5">here</font>
              </a>
              !
            </h5>
            <h5 className="text-center">
              New? Sign-up{" "}
              <a href="/register">
                <font color="#06b1e5">here</font>
              </a>
              !
            </h5>
          </div>
        </div>
      </div>
    );
  }
}

export default Landing;
