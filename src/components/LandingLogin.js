import React, { Component } from "react";
import logo from "../logo_sml.png";
import { Image } from "react-bootstrap";

class LandingLogin extends Component {
  render() {
    return (
      <div className="container">
        <div className="jumbotron mt-5">
          <div className="col-sm-8 mx-auto">
            <h1 className="text-center">
              <font color="#06b1e5">WELCOME TO THE ZIETO LOGIN PAGE</font>{" "}
              <br />
              <Image src={logo} />{" "}
            </h1>
          </div>
        </div>
      </div>
    );
  }
}

export default LandingLogin;
