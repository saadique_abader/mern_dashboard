import React, { Component } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";

class SubmissionLookup extends Component {
  render() {
    return (
      <Form>
        <br />
        <h2>Submission Lookup</h2>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Service Provider ID
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Service Provider ID"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Reference Number
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Reference Number"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Destination ID
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Destination ID"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Vendor ID
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder="Choose Vendor ID">
              <option />
              <option>Aid for AIDS</option>
              <option>Alien Software </option>
              <option>Alliance Management System</option>
              <option>Ampath</option>
              <option>Apace</option>
              <option>Apollo</option>
              <option>BI Solutions</option>
              <option>Bestmed</option>
              <option>Britton Development Services (PTY) LTD</option>
              <option>Cape Western Cape</option>
              <option>Carecross</option>
              <option>Carecross MME</option>
              <option>Chronic Medicine Dispensary</option>
              <option>Clicks</option>
              <option>Compharm</option>
              <option>Compuassist</option>
              <option>Computerkit (CKS) </option>
              <option>Datamax</option>
              <option>Denis Information</option>
              <option>Dental Information Systems (Pty) Ltd</option>
              <option>Dental Risk Company</option>
              <option>Digital Healthcare Switch</option>
              <option>Direct Medicines</option>
              <option>Diverse IT Marconi</option>
              <option>E-md</option>
              <option>Emergency Marcet Healtchcare</option>
              <option>Europ Assistance</option>
              <option>FlowCentric</option>
              <option>Goodx</option>
              <option>IKAT</option>
              <option>Health263</option>
              <option>Healthbridge</option>
              <option>ICW Consultants CC</option>
              <option>Iso Leso Optics Limited</option>
              <option>Keyhealth</option>
              <option>Lancet Laboratories</option>
              <option>Lenasia Computer Services</option>
              <option>Liberate</option>
              <option>Liberate Capture</option>
              <option>Life HealthCare</option>
              <option>Carecross</option>
              <option>Carecross MME</option>
              <option>Chronic Medicine Dispensary</option>
              <option>Clicks</option>
              <option>Compharm</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Member Number
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Member Number"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            External Number
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter External Number"
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Authorisation Number
          </Form.Label>
          <Col sm={6}>
            <Form.Control
              size="sm"
              type="text"
              placeholder="Enter Authorisation Number"
            />
          </Col>
        </Form.Group>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Administrator
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option>De Beers Benefit Society</option>
              <option>Momentum Africa </option>
              <option>OHI</option>
              <option>Pro Sano</option>
              <option>Profmed Medical Scheme Administrator</option>
              <option>Sanlam Health</option>
              <option>Spes Bona Financial Administrators (Pty) Ltd.</option>
              <option>VMED</option>
              <option>VMED Africa</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Scheme Plan
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option>De Beers Benefit Society</option>
              <option>Momentum Africa </option>
              <option>OHI</option>
              <option>Pro Sano</option>
              <option>Profmed Medical Scheme Administrator</option>
              <option>Sanlam Health</option>
              <option>Spes Bona Financial Administrators (Pty) Ltd.</option>
              <option>VMED</option>
              <option>VMED Africa</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Administrator
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option>De Beers Benefit Society</option>
              <option>Momentum Africa </option>
              <option>OHI</option>
              <option>Pro Sano</option>
              <option>Profmed Medical Scheme Administrator</option>
              <option>Sanlam Health</option>
              <option>Spes Bona Financial Administrators (Pty) Ltd.</option>
              <option>VMED</option>
              <option>VMED Africa</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Administrator
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option>De Beers Benefit Society</option>
              <option>Momentum Africa </option>
              <option>OHI</option>
              <option>Pro Sano</option>
              <option>Profmed Medical Scheme Administrator</option>
              <option>Sanlam Health</option>
              <option>Spes Bona Financial Administrators (Pty) Ltd.</option>
              <option>VMED</option>
              <option>VMED Africa</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
            Administrator
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option>De Beers Benefit Society</option>
              <option>Momentum Africa </option>
              <option>OHI</option>
              <option>Pro Sano</option>
              <option>Profmed Medical Scheme Administrator</option>
              <option>Sanlam Health</option>
              <option>Spes Bona Financial Administrators (Pty) Ltd.</option>
              <option>VMED</option>
              <option>VMED Africa</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Button variant="primary" type="submit">
          Submit
        </Button>
      </Form>
    );
  }
}

export default SubmissionLookup;
