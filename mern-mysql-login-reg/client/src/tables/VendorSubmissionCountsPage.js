import React, { Component } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";

class VendorSubmissionCounts extends Component {
  render() {
    return (
      <Form>
        <br />
        <h4>Vendor Submission Counts Page</h4>
        <br></br>
        <p>Please select the month for which you want the vendor counts</p>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}>
          Month
          </Form.Label>
          <Col sm={6}>
            <Form.Control size="sm" as="select" placeholder=" ">
              <option />
              <option value="2019-02-01">2019-02</option>
              <option value="2019-01-01">2019-01</option>
              <option value="2018-12-01">2018-12</option>
              <option value="2018-11-01">2018-11</option>
              <option value="2018-10-01">2018-10</option>
              <option value="2018-09-01">2018-09</option>
              <option value="2018-08-01">2018-08</option>
              <option value="2018-07-01">2018-07</option>
              <option value="2018-06-01">2018-06</option>
              <option value="2018-05-01">2018-05</option>
              <option value="2018-04-01">2018-04</option>
              <option value="2018-03-01">2018-03</option>
              <option value="2018-02-01">2018-02</option>
              <option value="2018-01-01">2018-01</option>
              <option value="2017-12-01">2017-12</option>
              <option value="2017-11-01">2017-11</option>
              <option value="2017-10-01">2017-10</option>
              <option value="2017-09-01">2017-09</option>
              <option value="2017-08-01">2017-08</option>
              <option value="2017-07-01">2017-07</option>
              <option value="2017-06-01">2017-06</option>
              <option value="2017-05-01">2017-05</option>
              <option value="2017-04-01">2017-04</option>
              <option value="2017-03-01">2017-03</option>
              <option value="2017-02-01">2017-02</option>
              <option value="2017-01-01">2017-01</option>
              <option value="2016-12-01">2016-12</option>
              <option value="2016-11-01">2016-11</option>
              <option value="2016-10-01">2016-10</option>
              <option value="2016-09-01">2016-09</option>
              <option value="2016-08-01">2016-08</option>
              <option value="2016-07-01">2016-07</option>
              <option value="2016-06-01">2016-06</option>
              <option value="2016-05-01">2016-05</option>
              <option value="2016-04-01">2016-04</option>
              <option value="2016-03-01">2016-03</option>
              <option value="2016-02-01">2016-02</option>
              <option value="2016-01-01">2016-01</option>
              <option value="2015-12-01">2015-12</option>
              <option value="2015-11-01">2015-11</option>
              <option value="2015-10-01">2015-10</option>
              <option value="2015-09-01">2015-09</option>
              <option value="2015-08-01">2015-08</option>
              <option value="2015-07-01">2015-07</option>
              <option value="2015-06-01">2015-06</option>
              <option value="2015-05-01">2015-05</option>
              <option value="2015-04-01">2015-04</option>
              <option value="2015-03-01">2015-03</option>
              <option value="2015-02-01">2015-02</option>
              <option value="2015-01-01">2015-01</option>
              <option value="2014-12-01">2014-12</option>
              <option value="2014-11-01">2014-11</option>
              <option value="2014-10-01">2014-10</option>
              <option value="2014-09-01">2014-09</option>
              <option value="2014-08-01">2014-08</option>
              <option value="2014-07-01">2014-07</option>
              <option value="2014-06-01">2014-06</option>
              <option value="2014-05-01">2014-05</option>
              <option value="2014-04-01">2014-04</option>
              <option value="2014-03-01">2014-03</option>
              <option value="2014-02-01">2014-02</option>
              <option value="2014-01-01">2014-01</option>
              <option value="2013-12-01">2013-12</option>
              <option value="2013-11-01">2013-11</option>
              <option value="2013-10-01">2013-10</option>
              <option value="2013-09-01">2013-09</option>
              <option value="2013-08-01">2013-08</option>
              <option value="2013-07-01">2013-07</option>
              <option value="2013-06-01">2013-06</option>
              <option value="2013-05-01">2013-05</option>
              <option value="2013-04-01">2013-04</option>
              <option value="2013-03-01">2013-03</option>
              <option value="2013-02-01">2013-02</option>
              <option value="2013-01-01">2013-01</option>
              <option value="2012-12-01">2012-12</option>
              <option value="2012-11-01">2012-11</option>
              <option value="2012-10-01">2012-10</option>
              <option value="2012-09-01">2012-09</option>
              <option value="2012-08-01">2012-08</option>
              <option value="2012-07-01">2012-07</option>
              <option value="2012-06-01">2012-06</option>
              <option value="2012-05-01">2012-05</option>
              <option value="2012-04-01">2012-04</option>
              <option value="2012-03-01">2012-03</option>
              <option value="2012-02-01">2012-02</option>
              <option value="2012-01-01">2012-01</option>
              <option value="2011-12-01">2011-12</option>
              <option value="2011-11-01">2011-11</option>
              <option value="2011-10-01">2011-10</option>
              <option value="2011-09-01">2011-09</option>
              <option value="2011-08-01">2011-08</option>
              <option value="2011-07-01">2011-07</option>
              <option value="2011-06-01">2011-06</option>
              <option value="2011-05-01">2011-05</option>
              <option value="2011-04-01">2011-04</option>
              <option value="2011-03-01">2011-03</option>
              <option value="2011-02-01">2011-02</option>
              <option value="2011-01-01">2011-01</option>
              <option value="2010-12-01">2010-12</option>
              <option value="2010-11-01">2010-11</option>
              <option value="2010-10-01">2010-10</option>
              <option value="2010-09-01">2010-09</option>
              <option value="2010-08-01">2010-08</option>
              <option value="2010-07-01">2010-07</option>
              <option value="2010-06-01">2010-06</option>
              <option value="2010-05-01">2010-05</option>
              <option value="2010-04-01">2010-04</option>
              <option value="2010-03-01">2010-03</option>
              <option value="2010-02-01">2010-02</option>
              <option value="2010-01-01">2010-01</option>
              <option value="2009-12-01">2009-12</option>
              <option value="2009-11-01">2009-11</option>
              <option value="2009-10-01">2009-10</option>
              <option value="2009-09-01">2009-09</option>
              <option value="2009-08-01">2009-08</option>
              <option value="2009-07-01">2009-07</option>
              <option value="2009-06-01">2009-06</option>
              <option value="2009-05-01">2009-05</option>
              <option value="2009-04-01">2009-04</option>
              <option value="2009-03-01">2009-03</option>
              <option value="2009-02-01">2009-02</option>
              <option value="2009-01-01">2009-01</option>
              <option value="2008-12-01">2008-12</option>
              <option value="2008-11-01">2008-11</option>
              <option value="2008-10-01">2008-10</option>
              <option value="2008-09-01">2008-09</option>
              <option value="2008-08-01">2008-08</option>
              <option value="2008-07-01">2008-07</option>
              <option value="2008-06-01">2008-06</option>
              <option value="2008-05-01">2008-05</option>
              <option value="2008-04-01">2008-04</option>
              <option value="2008-03-01">2008-03</option>
              <option value="2008-02-01">2008-02</option>
              <option value="2008-01-01">2008-01</option>
              <option value="2007-12-01">2007-12</option>
              <option value="2007-11-01">2007-11</option>
              <option value="2007-10-01">2007-10</option>
              <option value="2007-09-01">2007-09</option>
              <option value="2007-08-01">2007-08</option>
              <option value="2007-07-01">2007-07</option>
              <option value="2007-06-01">2007-06</option>
              <option value="2007-05-01">2007-05</option>
              <option value="2007-04-01">2007-04</option>
              <option value="2007-03-01">2007-03</option>
              <option value="2007-02-01">2007-02</option>
              <option value="2007-01-01">2007-01</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Button variant="primary" type="submit">
          Lookup
        </Button>
      </Form>
    );
  }
}

export default VendorSubmissionCounts;
