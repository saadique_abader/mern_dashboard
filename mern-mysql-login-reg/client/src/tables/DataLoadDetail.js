import React, { Component } from "react";
import { Form, Button, Row, Col } from "react-bootstrap";

class DataLoadDetails extends Component {
  render() {
    return (
      <Form>
        <br />
        <h2>Data Load Details</h2>
        <br />
        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}> Data File Serial </Form.Label>

          <Col sm={6}>
            <Form.Control size="sm" type="text" placeholder="Enter Data File Serial"/>
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formHorizontalEmail">
          <Form.Label column sm={2}> Data Load Serial </Form.Label>
            <Col sm={6}>
                <Form.Control size="sm" type="text" placeholder="Enter Data Load Serial"/>
            </Col>
        </Form.Group>

        <Button variant="primary" type="submit">
          View
        </Button>
      </Form>
    );
  }
}

export default DataLoadDetails;
