import React, { Component } from "react";

class Datasync extends Component{
    render(){
        return(
            <div>
                <br />
                     <h2>Datasync Check</h2>
                <br />
                <div>PDB 1 (piterak): 9069628</div>
                <table className= "col-md-5">
                    <thead>
                        <th>
                            <b>Server</b>
                        </th>
                        <th>
                            <b>Status</b>
                        </th>
                        <th>
                            <b>Oldest</b>
                        </th>
                        <th>
                            <b>Total</b>
                        </th>
                    </thead>
                    <tbody>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                    </tbody>
                </table>

                <div><a href="#" onclick="fix(&quot;piterak&quot;); return false;">Fix F</a></div>
                <div><a href="#" onclick="reset(&quot;piterak&quot;); return false;">Reset H</a></div>

                <br></br>
                <hr color="black"></hr>
                <br></br>

                <div>PDB 2 (purga): 484242</div>
                <table className= "col-md-5">
                    <thead>
                        <th>
                            <b>Server</b>
                        </th>
                        <th>
                            <b>Status</b>
                        </th>
                        <th>
                            <b>Oldest</b>
                        </th>
                        <th>
                            <b>Total</b>
                        </th>
                    </thead>
                    <tbody>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                    </tbody>
                </table>

                <div><a href="#" onclick="fix(&quot;piterak&quot;); return false;">Fix F</a></div>
                <div><a href="#" onclick="reset(&quot;piterak&quot;); return false;">Reset H</a></div>

                <br></br>
                <hr color ="black"></hr>
                <br></br>

                <div>MDB (sharki): 14133602</div>
                <table className= "col-md-5">
                    <thead>
                        <th>
                            <b>Server</b>
                        </th>
                        <th>
                            <b>Status</b>
                        </th>
                        <th>
                            <b>Oldest</b>
                        </th>
                        <th>
                            <b>Total</b>
                        </th>
                    </thead>
                    <tbody>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                        <tr> <td></td> <td></td> <td></td> <td></td> </tr>
                    </tbody>
                </table>

                <div><a href="#" onclick="fix(&quot;piterak&quot;); return false;">Fix F</a></div>
                <div><a href="#" onclick="reset(&quot;piterak&quot;); return false;">Reset H</a></div>
            </div>
        )
    }
}

export default Datasync;